import React, { Component } from 'react';
import MultiItemSelectorList from './MultiItemSelectorList';



class App extends Component {
    state = { 
        isPickingIngredient: false,
        ingredientIdBeingHeld: '',
        ingredientsSelected:  {}
    };


    demoData = {
        Cheese: {
            name: 'Cheese',
            id: 'A16jigau'
        },
        Bread: {
            name: 'Bread',
            id: 'V8aaFbCv'
        },
        Tomato: {
            name: 'Tomato',
            id: 'GDV9Nxv1'
        },
        Ham: {
            name: 'Ham',
            id: 'pPMyVQkC'
        },
        Sugar: {
            name: 'Sugar',
            id: 'E9n33Be4'
        },
        Flour: {
            name: 'Flour',
            id: '8NJgh3hy'
        },
        Eggs: {
            name: 'Eggs',
            id: '8mjfDS24'
        },
        Banana: {
            name: 'Banana',
            id: 'i9h6FH5w'
        },
        Potato: {
            name: 'Potato',
            id: '7icX73Fy'
        },
        Steak: {
            name: 'Steak',
            id: '8Thf0cmB'
        },
        Cucumber: {
            name: 'Cucumber',
            id: '7Jnmm5Cv'
        },
        
    };


    /**
     * A method to be passed to the MultiItemSelectorList
     * to handle the updating of ingredients in this class.
     */
    updateSelected = ( updatedList ) => {
        this.setState({ ingredientsSelected: updatedList });
    }



    /**
     * Default render method
     */
    render() {
        return (
            <div style={{ 
                margin: '0 auto',
                paddingTop: 50,
                width: 360,
             }}>

                <MultiItemSelectorList 
                    optionList={ Object.values(this.demoData) } 
                    selectedItems={ this.state.ingredientsSelected }
                    updateSelected={ this.updateSelected }
                />

            </div>
        );
    }
}


export default App;