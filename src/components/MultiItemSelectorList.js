import React, { Component } from 'react';
import _ from 'lodash';


/**
 * Styling configuration
 */
const styles = {
    container: {
        marginBottom: 30,
        boxShadow: 'rgba(0, 0, 0, 0.3) 0px 2px 6px 1px'
    },
    searchWrap: {
        padding: 10,
        borderBottom: '1px solid #999'
    },
    searchBar: {
        width: '100%',
        padding: 6,
        boxSizing: 'border-box',
        border: 'none'
    },
    optionWrap: {
        maxHeight: 230, 
        overflowY: 'auto'
    },
    option: {
        border: 'none',
        borderBottom: '1px solid #dddddd',
        display: 'block', 
        padding: 10, 
        cursor: 'pointer',
        backgroundColor: 'ffffff',
    },
    addedHeader: {
        borderTop: '1px solid #dddddd',
        borderBottom: '1px solid #dddddd',
        maxheight: 140, 
        overflowY: 'auto',
        padding: '14px 10px', 
        backgroundColor: '#eeeeee',
        fontWeight: 'bold'
    },
    addedItem: {
        backgroundColor: 'ffffff',
        border: 'none',
        borderBottom: '1px solid #bbb',
        display: 'block', 
        padding: 10,
        cursor: 'pointer'
    }
};


/**
 * A component for allowing a user to easily select multiple items in varying quantities.
 * 
 * @param {Object[]} optionList - Options to select from
 * @param {string} optionList[].name - Label
 * @param {string} optionList[].id - Unique identifer
 * @param {Object[]} selectedItems - Options that are selected
 * @param {string} selectedItems[].name - Label
 * @param {string} selectedItems[].id - Unique identifer
 * @param {function} updateSelected - Function to call on selection change
 */
class MultiItemSelectorList extends Component {
    state = { 
        searchString: '',
        selectedItems: {}
    };


    /**
     * Add selecected option to the selected items menu when an item 
     * is picked from the available options.
     */
    onSelectItem = (e) => {
        let name = e.target.attributes.name.value;
        let quantity = 1;

        let selectedItems = { 
            ...this.props.selectedItems,
            [name]: { name, quantity }
        };

        this.props.updateSelected(selectedItems)
    }


    /**
     * Removes an item from the selected list menu.
     */
    onRemoveItem = (e) => {
        e.preventDefault();
        
        let name = e.target.attributes.name.value;

        // omit the item from list
        let selectedItems = _.omit(this.props.selectedItems, [name]);

        // update the state
        this.props.updateSelected( selectedItems );
    }


    /**
     * Controlled input for quantity of each selected number pickers.
     */
    onItemNumberChange = (e) => {
        let name = e.target.attributes.name.value;
        let quantity = e.target.value;

        // Only update if the value is greater than 0
        if (0 < quantity) {
            let selectedItems = { 
                ...this.props.selectedItems,
                [name]: { name, quantity }
            };

            this.props.updateSelected( selectedItems );
        }

    }


    /**
     * A helper method for determining if an item is in the 
     * list of selected items.
     */
    hasItemInSelected = (name) => {
        if (Object.keys(this.props.selectedItems).indexOf(name) >= 0) {
            return name;
        }
    }


    /**
     * Responsible for filtering and rendering to the screen 
     * the available options to choose from.
     */
    renderChoices = () => {
        // Get the list options
        let options = this.props.optionList;

        // If there are no list options
        if (options.length === 0) {
            return <p>Please add some options to choose from.</p>;
        } 
        
        // If there are list options available
        else {
            // Filter the list based on search string. Convert to lowercase to ensure cross-capitalisation matching.
            let filteredList = options.filter( item => {
                let itemName = item.name.toLowerCase();
                let query = this.state.searchString.toLowerCase();

                return itemName.includes(query) && !this.hasItemInSelected(item.name);
            })
            
            // Order the list in alphabetical order
            filteredList.sort( (a, b) => { return a.name - b.name} );

            // Then render the available options of the list
            return filteredList.map(option => {
                return (
                    <div 
                        style={styles.option} 
                        onClick={ this.onSelectItem }
                        name={option.name}
                    >
                        {option.name}
                    </div>
                );
            });
        }
    }


    /**
     * Responsible for rendering the options that have been selected.
     */
    renderSelected = () => { 
        let selectedItemKeys = Object.keys(this.props.selectedItems);

        if (selectedItemKeys.length > 0) {
            return selectedItemKeys.map( key => {
                let item = this.props.selectedItems[key];
                return (
                    <div style={styles.addedItem} >
                        <input 
                            type="number" 
                            name={ item.name }
                            value={ this.props.selectedItems[key].quantity }
                            onChange={ this.onItemNumberChange }
                            style={{ maxWidth: 50, marginRight: 12 }}
                        />
                        {item.name} 
                        {item.quantifier}
                        <button
                            style={{ float: 'right', cursor: 'pointer' }}
                            name={ item.name } 
                            onClick={ this.onRemoveItem } 
                        >
                            X
                        </button>
                    </div>
                );
            }) ;   
        }

        else {
            return (
                <div style={{ ...styles.addedItem, color: '#888', textAlign: 'center' }} >You have not chosen anything...</div>
            );
        }
    }


    /** 
     * The default render method. 
     */
    render() {
        return(
            <div style={ styles.container } >
                <div style={styles.searchWrap} >
                    <input 
                        style={styles.searchBar}
                        type="text" 
                        value={ this.state.searchString } 
                        onChange={ e => this.setState({ searchString: e.target.value }) }
                        placeholder="Search for an item..." 
                    />
                </div>
                <div style={styles.optionWrap} >
                    {this.renderChoices()}
                </div>
                <div style={styles.addedHeader}>
                    Added:
                </div>
                {this.renderSelected()}
            </div>
        );
    }
}



export default MultiItemSelectorList;